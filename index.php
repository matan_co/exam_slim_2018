<?php
require "bootstrap.php";
use Api\Models\User;
use Api\Models\Product;
use Api\Middleware\Logging;
use Api\Middleware\Cors;

$app = new \Slim\App(['settings' => ['displayErrorDetails' => true]]);
$app ->add(new logging());
//---------------------------------------------------------------------------------------------------------------------------------------------------------------- //
//----------------------------------------------------------------- Users  --------------------------------------------------------------------------------------- //
$app->get('/users', function($request, $response,$args){    
    $_user = new User();
    $users = $_user->all();
    $payload=[];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=> $usr->id,
            'name'=> $usr->name,
            'phone'=> $usr->phone
        ];
    }
    return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
});

$app->get('/users/{user_id}', function($request, $response,$args){    
    $_user = User::find($args['user_id']);

    $payload=[];

    if($_user->id){
      return $response->withStatus(200)->withJson(json_decode($_user))->withHeader('Access-Control-Allow-Origin', '*');
    }else{
      return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }  
    
});

$app->post('/users', function($request, $response,$args){
    $name  = $request->getParsedBodyParam('name','');
    $phone = $request->getParsedBodyParam('phone','');    
    $_user = new User();
    $_user->name    =  $name;
    $_user->phone   =  $phone;
    $_user->save();
    if($_user->id){
        $payload = ['user_id'=>$_user->id];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});

$app->delete('/users/delete/{user_id}', function($request, $response,$args){
    $_user = User::find($args['user_id']); 
    $_user->delete();
    if($_user->exist){
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(200)->withHeader('Access-Control-Allow-Origin', '*');
        
    }
});

$app->put('/users/{user_id}', function($request, $response,$args){
    $name   = $request->getParsedBodyParam('name','');
    $phone  = $request->getParsedBodyParam('phone','');    
    $_user = User::find($args['user_id']);
    $_user ->name =$name;
    $_user ->phone =$phone;
    if( $_user->save()){
        $payload = ['user_id'=>$_user->id,"result"=>"The user was updated successfuly"];
        return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});

$app->post('/users/bulk', function($request, $response,$args){
    $payload =$request->getParsedBody();
    User::insert($payload);
    return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }

);

//---------------------------------------------------------------------------------------------------------------------------------------------------------------- //
//----------------------------------------------------------------- Users  --------------------------------------------------------------------------------------- //

//---------------------------------------------------------------------------------------------------------------------------------------------------------------- //
//----------------------------------------------------------------- Products  ------------------------------------------------------------------------------------ //

$app->get('/products', function($request, $response, $args){    
    $_product = new Product();
    $products = $_product->all();
    $payload=[];
    foreach($products as $prd){
        $payload[$prd->id] = [
            'id'=> $prd->id,
            'name'=> $prd->name,
            'price'=> $prd->price
        ];
    }
    return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
});

$app->post('/products/search', function($request, $response, $args){    
    $name       = $request->getParsedBodyParam('name',''); 
    $products  = Product::where('name', '=', $name)->get(); 
    $payload=[];
    foreach($products as $prd){
        $payload[$prd->id] = [
            'id'=> $prd->id,
            'name'=> $prd->name,
            'price'=> $prd->price
        ];
    }

    return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
});

$app->get('/products/{product_id}', function($request, $response, $args){    
    $_product = Product::find($args['product_id']);

    $payload=[];

    if($_product->id){
      return $response->withStatus(200)->withJson(json_decode($_product))->withHeader('Access-Control-Allow-Origin', '*');
    }else{
      return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }  
    
});

$app->post('/products', function($request, $response, $args){
    $name  = $request->getParsedBodyParam('name','');
    $price = $request->getParsedBodyParam('price','');    
    $_product = new Product();
    $_product->name    =  $name;
    $_product->price   =  $price;
    $_product->save();
    if($_product->id){
        $payload = ['product_id'=>$_product->id];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});

$app->delete('/products/delete/{product_id}', function($request, $response, $args){
    $_product = Product::find($args['product_id']); 
    $_product->delete();
    if($_product->exist){
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }else{
        return $response->withStatus(200)->withHeader('Access-Control-Allow-Origin', '*');   
    }
});

$app->put('/products/{product_id}', function($request, $response, $args){
    $name   = $request->getParsedBodyParam('name','');
    $price  = $request->getParsedBodyParam('price','');    
    $_product = Product::find($args['product_id']);
    $_product ->name =$name;
    $_product ->price =$price;
    if( $_product->save()){
        $payload = ['product_id'=>$_product->id, "result"=>"The product was updated successfuly"];
        return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});

$app->post('/products/bulk', function($request, $response, $args){
    $payload =$request->getParsedBody();
    Product::insert($payload);
    return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }

);

//---------------------------------------------------------------------------------------------------------------------------------------------------------------- //
//----------------------------------------------------------------- Products  ------------------------------------------------------------------------------------ //


$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();

